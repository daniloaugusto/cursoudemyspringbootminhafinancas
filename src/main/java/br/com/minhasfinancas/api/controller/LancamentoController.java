package br.com.minhasfinancas.api.controller;

import java.util.List;
import java.util.Optional;

import javax.persistence.Entity;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.minhasfinancas.api.dto.LancamentoDTO;
import br.com.minhasfinancas.exception.RegraNegocioException;
import br.com.minhasfinancas.model.entity.Lancamento;
import br.com.minhasfinancas.model.entity.Usuario;
import br.com.minhasfinancas.model.enums.StatusLancamento;
import br.com.minhasfinancas.model.enums.TipoLancamento;
import br.com.minhasfinancas.service.LancamentoService;
import br.com.minhasfinancas.service.UsuarioService;

@RestController
@RequestMapping("/api/lancamentos")
public class LancamentoController {

	private LancamentoService service;
	private UsuarioService usuarioService;
			//podemos passar dessa forma como abaixo com os parâmetros no construtor ou como abaixo comentado
	public LancamentoController(LancamentoService service, UsuarioService usuarioService) {
		this.service = service;
		this.usuarioService = usuarioService;
	}
	
		// Ou podemos passar dessa forma sem o contrutor e sem os parametros e adicionando uma annotation na classe
		    	//@RequiredArgsConstructor
	//private final UsuarioService usuarioService;
	//private final LancamentoService service; 
	
	@GetMapping
	public ResponseEntity buscar( 
			@RequestParam(value = "descricao", required = false) String descricao, //passando value =  e required = false
			@RequestParam(value = "mes", required = false) Integer mes,					//Não Obrigar que passe todos os parame
			@RequestParam(value = "ano", required = false) Integer ano,				//Para passar pela requisição de buscar,
			@RequestParam("usuario") Long idUsuario){						//Para evitar isso usamos o value e o required = false				
		Lancamento lancamentoFiltro = new Lancamento();					//Assim não precisaremos passar todos os parâmetros pra buscar
		lancamentoFiltro.setDescricao(descricao);
		lancamentoFiltro.setMes(mes);
		lancamentoFiltro.setAno(ano);
		
		Optional<Usuario> usuario = usuarioService.obterPorId(idUsuario);
		if(usuario.isPresent()){
		return ResponseEntity.badRequest().body("Não foi possível realizar a consulta. Usuario não encontrato para o Id Informado");
	}else {
		lancamentoFiltro.setId_usuario(usuario.get());
		}
		
		List<Lancamento> lancamentos = service.buscar(lancamentoFiltro);
		return ResponseEntity.ok(lancamentos);
	}
	
	
	@PostMapping("/salvar")
	public ResponseEntity salvar(@RequestBody LancamentoDTO dto) { //Json que vai ser convertido em Lancamento dto
		try {
			Lancamento entidade = converter(dto);
			entidade =  service.salvar(entidade);
			return new ResponseEntity(entidade, HttpStatus.CREATED);
		} catch (RegraNegocioException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}
	
	 						//passamos o id para carregar o id na variable e buscar os valores que devemos atualizar, 
	@PutMapping("{id}")		//>assim ele vai carregar na url /api/lancamentos/(id) ou o numero do ir
	public ResponseEntity atualizar( @PathVariable("{id}") Long id, @RequestBody LancamentoDTO dto) {
			return service.ObterPorId(id).map(entity -> {
				try {
					Lancamento lancamento = converter(dto);
					lancamento.setId(entity.getId());
					service.atualizar(lancamento);
					return ResponseEntity.ok(lancamento);
				}catch (RegraNegocioException e) {
					return ResponseEntity.badRequest().body(e.getMessage());
				}
		}).orElseGet( () -> 
			new ResponseEntity("Lancamento não encontrado na base de Dados.", HttpStatus.BAD_REQUEST ));
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity deletar(@PathVariable ("id") Long id) {
		return service.ObterPorId(id).map(entidade -> {
			service.deletar(entidade);
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}).orElseGet( () -> 
			new ResponseEntity("Lancamento não encontrado na base de Dados.", HttpStatus.BAD_REQUEST ));
	}
	
	private Lancamento converter(LancamentoDTO dto) {
		Lancamento lancamento = new Lancamento();
		lancamento.setId(dto.getId());
		lancamento.setDescricao(dto.getDescricao());
		lancamento.setAno(dto.getAno());
		lancamento.setMes(dto.getMes());
		lancamento.setValor(dto.getValor());
		
		Usuario usuario = usuarioService
			.obterPorId(dto.getUsuario())
			.orElseThrow( () -> new RegraNegocioException("Usuario não encontrato para o Id Informado"));
		
		lancamento.setId_usuario(usuario);
		lancamento.setTipo(TipoLancamento.valueOf(dto.getTipo()));
		lancamento.setStatus(StatusLancamento.valueOf(dto.getStatus()));
		
		return lancamento;
	}
}
