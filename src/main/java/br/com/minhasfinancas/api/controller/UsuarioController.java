package br.com.minhasfinancas.api.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.minhasfinancas.api.dto.UsuarioDTO;
import br.com.minhasfinancas.exception.ErroAutenticacao;
import br.com.minhasfinancas.exception.RegraNegocioException;
import br.com.minhasfinancas.model.entity.Usuario;
import br.com.minhasfinancas.service.UsuarioService;

@RestController
@RequestMapping("/api/usuarios")
public class UsuarioController {
	
	//Método Utilizado Somente para demonstrar como funciona, 
			//após subir a aplication abrimos o navegador e demos um localhost:8080/
					// o "/" é a url que digitamos depois do 8080
	/*@GetMapping("/") //Essa annotation está mapeando esté médodo para uma requisição com método Get para essa url
	public String helloWord() {
		return "hello word!";
	}*/
	//Estamos adicionando a interface porque o containner de injeção de dependencia do Spring vai procurar uma implementação e vai adicionar aqui.
	private UsuarioService service;
	
	//Adicionando o Construtor para a injeção de dependencias, ou sejá quando eu precisar chamar eu chamo meu contrutor
	public UsuarioController(UsuarioService service) {
		this.service = service;
	}
	
	@PostMapping("/autenticar")
	public ResponseEntity autenticar (@RequestBody UsuarioDTO dto) {
		try {
			Usuario usuarioAutenticado = service.autenticar(dto.getEmail(), dto.getSenha());
			return ResponseEntity.ok(usuarioAutenticado);
		}catch (ErroAutenticacao e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
		
	}
	
	
	 //Essa Annotation @RequestBody, é a que vai dizer que o objeto GSON que vem da requisição com os dados do usuario sejá transformado no objeto. 
									//E com essa annotation exigimoa que os dados do usuario venham com os atributos
	@PostMapping("/salvar") //Como é uma requisição do tipo POST utilizamos a annotation @PostMapping	
	public ResponseEntity salvar( @RequestBody UsuarioDTO dto) {  									
		
		Usuario usuario = Usuario.builder()
				.nome(dto.getNome())
				.email(dto.getEmail())
				.senha(dto.getSenha())
				.build();
				
		try {
			Usuario usuarioSalvo = service.salvarUsuario(usuario);
			return new ResponseEntity(usuarioSalvo, HttpStatus.CREATED);
		}catch (RegraNegocioException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}
	
	
}
