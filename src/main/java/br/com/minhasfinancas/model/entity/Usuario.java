package br.com.minhasfinancas.model.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Table( name = "usuario" , schema = "financas")
//@Getter
//@Setter
//@EqualsAndHashCode //hashCode e equals server para nos auxiliar na comparação de objetos
//@ToString //Facilitar o Debug - Quando tiver uma instancia basta passar o mouse em cima e será possivel ver uma instancia
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
//o Data Substitui as Anottations AllArgsConstructor,NoArgsConstructor, ToString, EqualsAndHashCode, Setter, Getter - Essas anotações removem a necessidade de criação dos Getter, Setter, ToString, EqualsAndHashCode
public class Usuario {

	@Id
	@Column(name = "id")
	@GeneratedValue( strategy = GenerationType.IDENTITY ) //Fazendo essa annotation o banco mesmo se encarrega de gerar o id
	private Long id;

	@Column(name ="nome")
	private String nome;

	@Column(name ="email")
	private String email;

	@Column(name ="senha")	
	private String senha;
	
	

}
