package br.com.minhasfinancas.model.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import br.com.minhasfinancas.model.enums.StatusLancamento;
import br.com.minhasfinancas.model.enums.TipoLancamento;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Table (name = "lancamento", schema = "financas")
//@Getter
//@Setter
//@EqualsAndHashCode //hashCode e equals server para nos auxiliar na comparação de objetos
//@ToString //Facilitar o Debug - Quando tiver uma instancia basta passar o mouse em cima e será possivel ver uma instancia
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data //Substitui as Anottations @AllArgsConstructor,@NoArgsConstructor, @ToString, @EqualsAndHashCode, @Setter, @Getter - Essas anotações removem a necessidade de criação dos Getter, Setter, ToString, EqualsAndHashCode
public class Lancamento {
	
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )//Fazendo essa annotation o banco mesmo se encarrega de gerar o id
	@Column( name ="id")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "id_usuario")
	private Usuario id_usuario; //Inserindo o Obejto da outra table, referenciando com a table Usuario através do objeto usuario
	
	@Column(name = "descricao")
	private String descricao;
	
	@Column( name ="mes")
	private Integer mes;
	
	@Column( name ="ano")
	private Integer ano;
	
	@Column(name = "valor")
	private BigDecimal valor;
	
	@Column(name = "data_cadastro")
	@Convert(converter = Jsr310JpaConverters.InstantConverter.class)
	private LocalDate data_cadastro;
	
	@Column(name =  "tipo")//O nome tipo vem la no banco de dados que foi definido
	@Enumerated(value = EnumType.STRING)//Poderia usar o tipo Ordinal ao invés de String, se estivesse no tipo Ordinal seria a ordem da constante do Enum TipoLancamento, no caso Receita estaria na posição 0 e despesa na posição 1, porém vamos gravar como String, para caso alguém mexer na ordem não bagunçada
	private TipoLancamento tipo; //Tipo de lancamento é o método com os atributos constantes dentro do Enum TipoLancamento
	
	@Column(name = "status")//O nome Status vem la no banco de dados que foi definido
	@Enumerated(value = EnumType.STRING)//Poderia usar o tipo Ordinal ao invés de String, se estivesse no tipo Ordinal seria a ordem da constante do Enum TipoLancamento, no caso PENDENTE estaria na posição 0 e CANCELADO na posição 1 e EFETIVADO na posição 2, porém vamos gravar como String, para caso alguém mexer na ordem não bagunçada
	private StatusLancamento status;


}
