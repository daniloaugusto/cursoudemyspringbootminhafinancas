package br.com.minhasfinancas.model.enums;

public enum TipoLancamento {
	//Como são constantes sempre ficam em letra maiuscula dentro de um Enum, e recebem por parametros os valores abaixo
	RECEITA,
	DESPESA
}
