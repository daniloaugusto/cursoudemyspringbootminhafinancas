package br.com.minhasfinancas.model.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.minhasfinancas.model.entity.Usuario;


//Dessa forma como declarou será provido as operações simples como salvar, atualizar, deletar, consultar e assim por diante
//No caso abaixao declaramos a Nossa Entidade Usuário e o segunto é o tipo da chave primaria
//Em tempo de execução será implementado as operações
public interface UsuarioRepository extends JpaRepository<Usuario, Long>{
	
	//Ao utilizar findBy e a propriedade que estamos buscando como pos exemplo findByEmail 
		//ele vai buscas dentro da Entidade Usuário, dentro do UsuarioRepository,
			//então vai buscas na Entidade ou na Table do banco com o nome email como por exemplo e 
				//casos exista ele vai procurar o parâmetro que foi passado no caso, String email e executar 
					//a consulta na base de dados, evitando assim colocar o sql, pois o SpringData faz isso sozinho para nós  
	//Poderiamos tabém passar dois parâmtros, como por exemplo Optional<Usuario> findByEmailAndNome(String email,String nome);
		//Optional<Usuario> findByEmail(String email); - Esse aqui foi o primeiro exemplo criado no curso
	
	//Podemos fazer para retornar do tipo boolean como por exemplo
  	boolean existsByEmail(String email); //Isso aqui seria a mesma coisa de que se fizemos uma busca do usuário por E-mail como por exemplo 
  														//Select * from usuario where exists() buscanso um usuário com esse E-mail  
  	Optional<Usuario> findByEmail(String email); //Utilizando para buscar usuario por Email
  	
}







