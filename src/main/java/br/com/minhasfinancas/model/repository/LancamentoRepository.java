package br.com.minhasfinancas.model.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import br.com.minhasfinancas.model.entity.Lancamento;

//Dessa forma como declarou será provido as operações simples como salvar, atualizar, deletar, consultar e assim por diante
//No caso abaixao declaramos a Nossa Entidade Usuário e o segunto é o tipo da chave primaria
//Em tempo de execução será implementado as operações
public interface LancamentoRepository extends JpaRepository<Lancamento, Long>{

}
