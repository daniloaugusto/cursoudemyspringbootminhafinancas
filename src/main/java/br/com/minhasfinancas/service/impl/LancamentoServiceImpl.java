package br.com.minhasfinancas.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.ExampleMatcher.StringMatcher;
import org.springframework.stereotype.Service;

import br.com.minhasfinancas.exception.RegraNegocioException;
import br.com.minhasfinancas.model.entity.Lancamento;
import br.com.minhasfinancas.model.enums.StatusLancamento;
import br.com.minhasfinancas.model.repository.LancamentoRepository;
import br.com.minhasfinancas.service.LancamentoService;

//Após criado nossa interface LancamentoService, fazemos o implements da mesma e depois Utilizamos o Eclipse para criar os 
		//Métodos de forma automatica.
@Service
public class LancamentoServiceImpl implements LancamentoService{
	//Instancia de Lancamento Repository
	private LancamentoRepository repository;
	
	//Utilizamos o Construtor, e vai injetar. Não há necessidade de Adicionar a annotation @AutoWired, porque ele já injeta por ser um bin gerenciavel
	public LancamentoServiceImpl(LancamentoRepository repository) {
		this.repository = repository;
	}
	
	@Override
	@Transactional
	public Lancamento salvar(Lancamento lancamento) {
		validar(lancamento);
		lancamento.setStatus(StatusLancamento.PENDENTE);
		return repository.save(lancamento);	
	}

	@Override
	@Transactional
	public Lancamento atualizar(Lancamento lancamento) {
		validar(lancamento);
		Objects.requireNonNull(lancamento.getId()); //Restrição, para garantir que passe um lancamento com id, se não será lancado uma Exception
		return repository.save(lancamento);
	}

	@Override
	@Transactional
	public void deletar(Lancamento lancamento) {
		Objects.requireNonNull(lancamento.getId());
		repository.delete(lancamento);
		
	}

	@Override
	@Transactional
	public List<Lancamento> buscar(Lancamento lancamentoFiltro) {
		Example example = Example
				.of(lancamentoFiltro, ExampleMatcher
						.matching()
						.withIgnoreCase()
						.withStringMatcher(StringMatcher.CONTAINING)); //Vai fazer uma configuração para que quando chamamos o método por 
														//EXAMPLE, ele leve em consideração somente as propriedades que foram
															//Populadas.
							//ExampleMatcher habilita um método dentro dele que quando o campo é do tipo String 
		  						//Não importa se foi digitado em letra maiúscula ou minúscula, ele ignora usando o withIgnoreCase()
													//Buscando na base o valor que está lá
						//.withStringMatcher(null) é a forma de como ele vai utilizar pra buscar as String na base de dados
								//EX: eu digitei uma descrição do lancamento como filtro e foi digitado, apenas a letra A, então com esse recurso, 
							//podemos dizer pra encontre todos os lancamento que tenham a letra A no meio, no fim, no inicio,exato. Enfim depende a forma como desejar
							//Isso vai depender do que se utilizar dentro desse método, StringMatcher.CONTAINING, alterando essa funcionalidade, CONTAINING.
		return repository.findAll(example);
	}

	@Override
	public void atualizarStatus(Lancamento lancamento, StatusLancamento status) {
		lancamento.setStatus(status);
		atualizar(lancamento);
		
	}

	@Override
	public void validar(Lancamento lancamento) {
		
		//Método trim() remove os espações que foram digitados antes ou depois na String
		if (lancamento.getDescricao() == null || lancamento.getDescricao().trim().equals("")) {
			throw new RegraNegocioException("Informe uma Descrição Válida.");
		}
		if (lancamento.getMes() == null || lancamento.getMes() <1 || lancamento.getMes() > 12 ) {
			throw new RegraNegocioException("Informe um Mês Valido.");
			
		}//Verificando se o ano digitado possui 4 casas depois de ter convertido em String 
				// com a seguinte sintaxe, lancamento.getAno().toString().length() !=4)
		if (lancamento.getAno() == null || lancamento.getAno().toString().length() !=4) {
			throw new RegraNegocioException("Informe um Ano Válido.");
		}
		if (lancamento.getId_usuario() == null || lancamento.getId_usuario().getId() == null) {
			throw new RegraNegocioException("Informe um Usuário.");
		}
		if (lancamento.getValor() == null || lancamento.getValor().compareTo(BigDecimal.ZERO) < 1) {
			throw new RegraNegocioException("Informe um Valor Válido.");
		}
		if (lancamento.getTipo() == null) {
			throw new RegraNegocioException("Informe um tipo de Lançamento.");
		}
		
	}

	@Override
	public Optional<Lancamento> ObterPorId(Long id) {
		return repository.findById(id);
	}
		
		
}
	
