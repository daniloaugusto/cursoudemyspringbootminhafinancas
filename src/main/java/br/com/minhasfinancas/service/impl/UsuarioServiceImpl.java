package br.com.minhasfinancas.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.minhasfinancas.exception.ErroAutenticacao;
import br.com.minhasfinancas.exception.RegraNegocioException;
import br.com.minhasfinancas.model.entity.Usuario;
import br.com.minhasfinancas.model.repository.UsuarioRepository;
import br.com.minhasfinancas.service.UsuarioService;

@Service//Com essa anotação estamos dizendo para o container do Spring que gerencia a classe UsuarioServiceImpl, ou sejá ele vai criar uma instancia e gerar um container para quando precisarmos injetar em outras classes
public class UsuarioServiceImpl implements UsuarioService{

	//Como o UsuarioServiceImpl não pode acessar diretamente a base de dados ele vai precisar do UsuarioRepository para fazer tais operações 
	private UsuarioRepository repository;
	
	//Adicionar um construtor que vai receber como parâmetro a implementação do repository como parâmetro.
	//Para isso ir em Generate Construtor Using Filds, que o eclipse ira gerar automaticamente para nós
	
	@Autowired
	public UsuarioServiceImpl(UsuarioRepository repository) {
		super();
		this.repository = repository;
	}
	
	
	@Override
	public Usuario autenticar(String email, String senha) {
		Optional<Usuario> usuario = repository.findByEmail(email);//selecionei o repository.findByEmail(email); e dei um ALT + SHIFT + L e cria a variavel usuario e automaticamente o eclipse colocou pra gente Optional<Usuario> usuario =
		
		if (!usuario.isPresent()) {
			throw new ErroAutenticacao("Usuário não encontrado para o Email Informado.");
		}
		
		if (!usuario.get().getSenha().equals(senha)) {
			throw new ErroAutenticacao("Senha inválida.");
		}
		
		return usuario.get();
	}


	@Override
	@Transactional //Essa annotation vai criar na base dados uma transação, vai executar o método de salvar e depois de salvar vai commitar
	public Usuario salvarUsuario(Usuario usuario) {
		validarEmail(usuario.getEmail()); //Primeiramente validamos o E-mail do usuario que estamos tentando salvar pra garantir que ele não esteja cadastrado na base pra outro usuario
		return repository.save(usuario); //Aqui ele vai retornar a instancia do usuário já persistido na base com o ID 
	}

	@Override
	public void validarEmail(String email) {
		boolean existe = repository.existsByEmail(email);
		if (existe) {
			throw new RegraNegocioException("Já Existe um usuário Cadastrado com este email."); //Como RegraNegocioException não existe vamos criar uma classe, e pra ficar mais separado e organizado, vamos criar um pacote de Exception, clicando em cima do erro apresentado e alterar o nome do pacote. 
		}
	}


	@Override
	public Optional<Usuario> obterPorId(Long id) {
		return repository.findById(id);
	}
	
}
