package br.com.minhasfinancas.service;

import java.util.List;
import java.util.Optional;

import br.com.minhasfinancas.model.entity.Lancamento;
import br.com.minhasfinancas.model.enums.StatusLancamento;

public interface LancamentoService {
	//Aqui Vamos Criar o CRUD
	Lancamento salvar(Lancamento lancamento);
	
	Lancamento atualizar(Lancamento lancamento);
	
	void deletar(Lancamento lancamento);
	
	List<Lancamento>buscar(Lancamento lancamentoFiltro);
	
	void atualizarStatus(Lancamento lancamento, StatusLancamento status); //StatusLancamento do nosso Enum que criamos 
																					//com o status do lancamento
	void validar(Lancamento lancamento);
	
	Optional<Lancamento>ObterPorId(Long id);
}
