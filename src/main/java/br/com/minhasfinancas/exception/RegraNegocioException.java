package br.com.minhasfinancas.exception;
//DEixamos ela como RuntimeException para ela ser uma Exception em tempo de execução
public class RegraNegocioException extends RuntimeException {

	//Construtor
	public RegraNegocioException(String msg) {
		super(msg);
	}
}
