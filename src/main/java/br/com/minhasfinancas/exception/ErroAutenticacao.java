package br.com.minhasfinancas.exception;

public class ErroAutenticacao extends RuntimeException {
	
	//Adicionando o Construtor
	public ErroAutenticacao(String mensagem) {
		super(mensagem); //Passando o Construtor da Classe passando a mensagem
	}

}
