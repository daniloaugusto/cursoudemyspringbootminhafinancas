package br.com.minhasfinancas.service;




import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import br.com.minhasfinancas.exception.ErroAutenticacao;
import br.com.minhasfinancas.exception.RegraNegocioException;
import br.com.minhasfinancas.model.entity.Usuario;
import br.com.minhasfinancas.model.repository.UsuarioRepository;
import br.com.minhasfinancas.service.impl.UsuarioServiceImpl;



//Mocks > é um recurso que permite que criemos um objeto fake, ou instancia fake, de nossas clases, onde podemos simular a chamada de métodos e retorno de propriedades. 
      // Utilizamos este recurso para testar o comportamento de outros objetos.
		//A Classe Mockito do org.mockito, não precisamos adicionar mais nenhuma biblioteca adicional, pois a biblioteca Mockito já vem junto com com o módulo de Test do springBoot, que é o spring-boot-starter-test

@SpringBootTest //--Como foi mokado utilizando o Mockito não precisamos mais dessa annotation
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
public class UsuarioServiceTest {
	
	@SpyBean //Como não podemos mochar a classe UsuarioService pois precisamos chamar os métodos e mockando não deixaria, 
	UsuarioServiceImpl service;		//Para isso vamos usar o SpyBean,Como o service sempre chama os métodos originais, 
					//Usando o SpyBean estamos mockando esse métodos, e dizendo como vai ser o comportamento. 
					// Para isso vamos vamos trocar a instancia UsuarioService para a instancia Implementada UsuarioServiceImpl, 
							//Ou seja a instancia real 
	//@Autowired
	@MockBean
	UsuarioRepository repository; //chamando o usuário repostory, para garantir que não há nenhum usuário cadastrado com o E-mail se não vai lançar exceção
	
	
	@Test
	public void deveSalvarUmUsuario() {
		//Mockando o Método validar Email
		//1° Cenario
 		Mockito.doNothing().when(service).validarEmail(Mockito.anyString()); //Todos os métodos do nosso @SpayBen, devem ser feito dessa forma, 
 												//Porque primeiramente chamasse sempre o método real
 		Usuario usuario = Usuario.builder()
 				.id(1L)
 				.nome("nome")
 				.email("email@email.com")
 				.senha("senha")
 				.build();
 		//Mockar o cenário de salvar
 		Mockito.when(repository.save(Mockito.any(Usuario.class))).thenReturn(usuario); 				
 		//2° Acao
 		Usuario usuarioSalvo = service.salvarUsuario(new Usuario());
 		
 		//3° Verificação
 		Assertions.assertThat(usuarioSalvo).isNotNull();
 		Assertions.assertThat(usuarioSalvo.getId()).isEqualTo(1L);
 		Assertions.assertThat(usuarioSalvo.getNome()).isEqualTo("nome");
 		Assertions.assertThat(usuarioSalvo.getEmail()).isEqualTo("email@email.com");
 		Assertions.assertThat(usuarioSalvo.getSenha()).isEqualTo("senha"); 		
 		
	}	
	@Test
	public void naoDeveSalvarUsuarioComEmailJaCadastrado() {
		//1° Cenario
		//Vamos Criar um usuario
		String email= "email@email.com";
		Usuario usuario = Usuario.builder()
				.email(email)
				.build();
		Mockito.doThrow(RegraNegocioException.class).when(service).validarEmail(email);
		
 		//2° Acao
		//service.salvarUsuario(usuario);
		Throwable excepition = Assertions.catchThrowable( () -> service.salvarUsuario(usuario));//Lança uma excessão, caso tente salvar um usuario com e-mail já cadastrado.
		
 		//3° Verificação
		Mockito.verify(repository, Mockito.never()).save(usuario);
		
				
	}

	//Primeiro Teste de Autenticação
	@Test
	public void deveAutenticarUmUsuarioComSucesso() {
		//1° Cenario			
		String email = "email@email.com";
		String senha = "senha";
		
		Usuario usuario = Usuario.builder()
				.email(email)
				.senha(senha)
				.id(1l)
				.build();
		Mockito.when( repository.findByEmail(email)).thenReturn(Optional.of(usuario)); //Aqui ele vai retornar um Optional com o meu usuario aqui dentro, o usuario criado no cenário.
		
		//2° Acao
		Usuario result = service.autenticar(email, senha);
		
		//3° Verificação
		Assertions.assertThat(result).isNotNull();
	}
	
	@Test
	public void deveLancarErroQuandonaoEncontrarUsuarioCadastradoComOEmailInformado() {
		//1° Cenario	
		//Nesse cenário não importa o email que está sendo passado, sempre vamos retornar vazio.através do Optional.empty()
		Mockito.when(repository.findByEmail(Mockito.anyString())).thenReturn(Optional.empty());
		
		//2° Acao
		//Usando a Expressão Lambida
		Throwable excepition = Assertions.catchThrowable( () -> service.autenticar("email@email.com","senha")); //Dessa forma estamos verificando a excessão, tratando a  mensagem
		
		
		//2° Verificacao
		Assertions.assertThat(excepition)
		.isInstanceOf(ErroAutenticacao.class)
		.hasMessage("Usuário não encontrado para o Email Informado.");	//Vai funcionar como o Expeted semelhante ao Juni4
												//Um detalhe muito importante é
												//Essa mensagem do hasMessage deve ser igualzinha a da classe 
												//UsuarioServiceImpl nesse caso é "Usuário não encontrado."
																					
	}
	@Test
	public void deveLancarErroQuandoSenhaNaoBater() {
		//1° Cenario
		String senha = "senha"; //Criado essa variavel senha
		Usuario usuario = Usuario.builder()
				.email("email@email.com")
				.senha(senha)
				.build();
		Mockito.when(repository.findByEmail(Mockito.anyString())).thenReturn(Optional.of(usuario));
		
		
		//2° Acao
		Throwable excepition = Assertions.catchThrowable( () -> service.autenticar("email@email.com","123")); //Dessa forma estamos verificando a excessão, tratando a  mensagem
	
		//2° Verificacao
		Assertions.assertThat(excepition)
		.isInstanceOf(ErroAutenticacao.class)
		.hasMessage("Senha inválida.");	//Vai funcionar como o Expeted semelhante ao Juni4
										//Um detalhe muito importante é
										//Essa mensagem do hasMessage deve ser igualzinha a da classe se não o teste não passará 
										//UsuarioServiceImpl, nesse caso nosso é "Senha inválida.
	}
	
	//Como passamos a Utilizar a Annotation @SpyBen não precisamos mais desse método setUp e a annotation @BeforeEach
	//@Before > Utilizando desta forma, com essa annotation vai executar primeiramente este método antes dos nossos testes
	//>>>> Essa annotation e esse metodo SetUp não funcionou para mim
	/*@BeforeEach 
	public void setUp() {
				//Quando Utilizamos a annotation  @MockBean não precisamos mais injetar na mão como no exemplo: repository = Mockito.mock(UsuarioRepository.class), que também está abaixo. 
		//repository = Mockito.mock(UsuarioRepository.class); //Criado instancia Mocada do UsuarioRepository, dessa forma podemos chamadas fake pros médotos do repository
		service = new UsuarioServiceImpl(repository); //Criando a instancia Real do UsuarioService, e chamar os métodos reais do service.
	}*/
	//Médoto para validar o Email
	//@org.junit.Test(expected = org.junit.Test.None.class) // - No curso mandou fazer assim, mas deu erro 
	
	@Test
	public void deveValidarEmail() {
		//1° Cenario
		//Podemos dizer a Grosso modo que usuarioRepositoryMock é um cenário mocado, escondido.
		//Exemplo de como criar o Mock, UsuarioRepository usuarioRepositoryMock = Mockito.mock(UsuarioRepository.class); // Apartir desta instancia usuarioRepositoryMock,
																								//podemos simular a chamada dos médotos, deleteAll(); e save(usuario); 
		//1° Cenario																										//Facilitando a criação do cenário, sem a necessidade de executar de fato os métodos reais. 
		//>> repository.deleteAll(); -- Podemos remover esse método e chama-lo de outra forma, porque agora temos ele mocado, conforme abaixo.
	   Mockito.when(repository.existsByEmail(Mockito.anyString())).thenReturn(false);//Dessa forma quando chamamos o método existsByEmail, passando qualquer String vamos retornar false,
																								//Então Teoricamente não ira lançar nenhuma exceção
		
		//2° acao
		service.validarEmail("email@email.com");		
		
	}
	
	@Test
	public void deveLancarErroQuandoExistirEmailCadastrado() {
		//1° Cenario
		Mockito.when(repository.existsByEmail(Mockito.anyString())).thenReturn(true);//Nes
		//1° Cenario
		/*Usuario usuario = Usuario.builder()
				.nome("usuario")
				.email("email@email.com")
				.build();
		repository.save(usuario);*/ //> Primeiro Cenario montado Sem o Mokito
		
		//2° acao
		//service.validarEmail("email@email.com"); //Como implementei o Mockito não neste método, não é necessario, 
															//passar a validação porque já validei que retornou verdadeiro, 
													//Se fosse falso não iria passar no meu teste
																	

	}
}

