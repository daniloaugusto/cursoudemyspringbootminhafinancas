
package br.com.minhasfinancas.model.repository; //É necessário que o nome desse pacote seja semelhante ao repository da Entidade que vai ser testado
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import br.com.minhasfinancas.model.entity.Usuario;
//Para fazer o teste unitário ou de integração é necessário três elementos:
//> 1° Cenário > Cria-se o senário
//> 2° Ação/execução > Executa o médoto que está querendo testar
//> 3° Verificação > Depois verificamos se está como esperado.
//Todos os testes realizados na camada UsuarioRepository serão testes de integração, ou seja um teste que precisa de recursos externos a aplicação, ou seja 
//Como vamos ter que acessar a base de dados será necessário fazer um teste de integração

//@SpringBootTest Usando essa annotation, estamos testando inserindo dados na base de produção e não na base que é criada em tempo de execução.
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@DataJpaTest //Cria uma instancia do banco de dados na memória e ao final, ela faz um rollback e desfaz tudo que foi executado na transação
@AutoConfigureTestDatabase(replace = Replace.NONE)//Usando essa annotation ele não cria uma instancia própria fo banco de teste em memória, e não sobscreve as configurações que estão no application-test.properties 
public class UsuarioRepositoryTest {
	
	String emailDeTest ="email@email.com";
	
	@Autowired //Para que o Spring Boot injete para nós essa instancia da interface
	UsuarioRepository repository;

	@Autowired
	TestEntityManager entityManager;  //entityManager > Classe responsavel por fazer as operações na base de Dados, como persistir, salvar, deletar e etc...
														//Vamos usar o entityManager pra fazer a manipulação de cenário
												//>>>> por ter o TestEntityManager antes do entityManager, não é o entityManager original é apenas para testes
	@Test
	public void deveVerificarAExistenciaDeUmEmail( ) {		
		// > 1° Cenário
		/*Usuario usuario = Usuario.builder() //Usuario usuario = usuario é uma instancia de usuário
				.nome("usuario")  //Criamos o usuario
				.email("email@email.com")
				.build();*/ // >>>Substituindo pelo códio abaixo porque criamos o metódo criarUsuario 
		Usuario usuario = criarUsuario(); // >>> Dessa forma substituimos pelo nosso build por causa do método.

		//> 1° Cenário > Cria-se o senário
		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>> repository.save(usuario); //usuario da instancia que fizemos, e salvando na base de dados
											//repository.save(usuario) > Refatorando, vamos eliminar esse cenario que foi criado com a própria classe que estamos testando
		//No lugar de repository.save(usuario) vamos usar o entityManager
		entityManager.persist(usuario);
		
		
		
		//> 2° Ação/execução > Executa o médoto que está querendo testar
		boolean result = repository.existsByEmail(emailDeTest); //verificando se existe o usuário na base de dados com este e-mail
		//> 3° Verificação > Depois verificamos se está como esperado.
		Assertions.assertThat(result).isTrue(); //Assertions, classe é da biblioteca assertj.core que já vem com o spring boot, está verificando se o valor
		//da váriavel passado na variavel result na ação é verdadeiro
	}

	@Test
	public void deveRetornarFalsoQuandoNaoHouverUsuarioCadastradoComEmail() {

		//> 1° Cenário 
		  ///repository.deleteAll();  >>> Utilizando o entityManager não precisamos mais deixar para deletar pois devido a annotation @DataJpaTest
													//vai ser aberto uma transação na base de dados vai executar todo o teste e ao encerrar ele da um rollback 
																	//Vai executar o teste acima e quando finalizar não preciso limpar porque ele faz automaticamente 
		//> 2° Ação/execução 
		boolean result = repository.existsByEmail(emailDeTest); 

		//> 3° Verificação
		Assertions.assertThat(result).isFalse();

	}
	
	@Test
	public void devePersistirUmUsuarioNaBaseDeDados() {
		// > 1° Cenário
		// > 1° Cenário
		/*Usuario usuario = Usuario.builder() //Usuario usuario = usuario é uma instancia de usuário
				.nome("usuario")  //Criamos o usuario
				.email("email@email.com")
				.build();*/ // >>>Substituindo pelo códio abaixo porque criamos o metódo criarUsuario 
		Usuario usuario = criarUsuario(); // >>> Dessa forma substituimos pelo nosso build por causa do método.
	
		// > 2° Ação/execução 
		Usuario usuarioSalvo = repository.save(usuario);
		
		
		// > 3° Verificação
		Assertions.assertThat(usuarioSalvo.getId()).isNotNull(); //usuarioSalvo.getId()) faz um get verificando se o usuario salvo contém id, não interesa o ID que está la 
	}
	
	@Test 
	public void deveBuscarUmUsuarioPorEmail() {		
		// > 1° Cenário
		/*Usuario usuario = Usuario.builder() //Usuario usuario = usuario é uma instancia de usuário
				.nome("usuario")  //Criamos o usuario
				.email("email@email.com")
				.build();*/ // >>>Substituindo pelo códio abaixo porque criamos o metódo criarUsuario 
		Usuario usuario = criarUsuario(); // >>> Dessa forma substituimos pelo nosso build por causa do método.
		entityManager.persist(usuario); //A instancia que está sendo passada no entityManager não pode ter id se não lança uma exceção
		
		// > 2° Ação/execução 
		Optional<Usuario> result = repository.findByEmail(emailDeTest);//selecionei o repository.findByEmail(email); e dei um ALT + SHIFT + L e cria a variavel usuario e automaticamente o eclipse colocou pra gente Optional<Usuario> usuario =
		
		// > 3° Verificação
		Assertions.assertThat(result.isPresent()).isTrue(); //Se o Resultado está presente então retorna verdadeiro
		
	}
	@Test 
	public void deveRetornarVazioAoBuscarUsuarioPorEmailQuandoNaoExisteNaBase() {		
		// > 1° Cenário
		   // Nesse caso não vamos criar senario para a base estar limpa
		
		// > 2° Ação/execução 
		Optional<Usuario> result = repository.findByEmail(emailDeTest);//selecionei o repository.findByEmail(email); e dei um ALT + SHIFT + L e cria a variavel usuario e automaticamente o eclipse colocou pra gente Optional<Usuario> usuario =
		
		// > 3° Verificação
		Assertions.assertThat(result.isPresent()).isFalse(); //Se o Resultado está presente então retorna verdadeiro
		
	}

	//Criando esse método para não ter que ficar repetindo código ai só chamar o cenario 
	public static Usuario criarUsuario() {
		return 	 Usuario.builder() //Usuario usuario = usuario é uma instancia de usuário
				.nome("usuario")  //Criamos o usuario
				.email("email@email.com")
				.senha("senha")
				.build();
		
	}
	

}
